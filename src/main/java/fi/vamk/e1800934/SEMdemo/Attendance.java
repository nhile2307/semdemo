package fi.vamk.e1800934.SEMdemo;

import java.io.Serializable;
import java.text.DateFormat;
import java.time.Instant;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;

@Entity
@NamedQuery(name = "Attendance.findAll", query = "SELECT p FROM Attendance p")
public class Attendance implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;
  private String key;
  private Instant date;

  public Attendance(String key) {
    date = Instant.now();
    this.key = key;
  }

  public Attendance() {

  }

  public void setDate(Instant date) {
    this.date = date;
  }

  public Instant getDate() {
    return this.date;
  }

  public void setId(int id) {
    this.id = id;
  }

  public int getId() {
    return this.id;
  }

  public void setKey(String key) {
    this.key = key;
  }

  public String getKey() {
    return this.key;
  }

  public String toString() {
    return id + " " + key + " " + date.toString();
  }
}
